pip install mod-wsgi

gunicorn --pid PID_FILE --bind 0.0.0.0:5000 wsgi:app

Steps follow for run flask_app

cd flask_app
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
gunicorn --pid PID_FILE --bind 0.0.0.0:5000 wsgi:app